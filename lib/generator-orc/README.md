Generates some common angular source files.

You should install yeoman globally to to use this generator:
    npm install yo -g
    npm link — in this directory

yo orc service.js -s              — generates angular service
yo orc some-controller.js -c      — generates angular controller
yo orc some-directive.js -d       — generates angular directive
yo orc some-controller.spec.js -S — generates jasmine tests

TODO: type options should not accept any value so they could be placed before a 
filename.
