( function () { 'use strict';


angular
    .module('list.listController', [
        'events',
        'data.model'
    ])
    .controller('ListController', ListController);


function ListController(model, events) {

    this.items = model;

    this.setCenter = function (item) {
        events.setCenter.fire(item);
    };

}


} () );
