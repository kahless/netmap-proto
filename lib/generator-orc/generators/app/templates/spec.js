'use strict';


describe('projectData', function () {


    var $rootScope, projectData;

    var records = 'data';
    var user = {
        name      : 'name',
        order     : 'order',
        propNames : 'propNames'
    };
    var project = {
        userName  : user.name,
        order     : user.order,
        propNames : user.propNames,
        records   : records
    };


    beforeEach( module('counter.data.projectData') );

    beforeEach( inject( function (_$rootScope_, _projectData_) {
        $rootScope  = _$rootScope_;
        projectData = _projectData_;
    } ) );

    beforeEach( inject( function ($q, userData, recordsData) {
        spyOn(userData, 'download')
            .and.returnValue( $q.resolve(user) );
        spyOn(recordsData, 'download')
            .and.returnValue( $q.resolve(records) );
    } ) );


    it('должен генерить событие готовности данных', function (done) {
        projectData.download()
            .then( function (data) {
                expect(data).toEqual(project);
                done();
            } );
        $rootScope.$apply();
    });


});
