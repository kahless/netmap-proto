( function () { 'use strict';


angular
    .module('widgets.editable.directive', [])
    .directive('xxEditable', xxEditable);


function xxEditable() {
    return {
        restrict: 'A',
        transclude: 'element',
        scope: {
            editing : '=?xxEditable',      // Переключатель состояния
            model   : '=?xxEditableModel', // Текущее значение для отображения
            render  : '=?xxEditableRender' // Генератор текста для preview
        },
        link(scope, element, attrs, ctrl, $transclude) {
            $transclude(
                transclude.proxy(scope, element, $transclude)
            );
        }
    };
}


function transclude(scope, element, $transclude, clone) {


    var preview = clone.find('.preview');
    var editor  = clone.find('.editor');
    var input   = clone.find('.input');


    preview.click(showInput);
    input.keyup(handleKey);

    scope.$watch( 'editing', editable => {
        editable
            ? showInput()
            : showPreview();
    } );

    scope.$watch( 'model', text => {
        if (!scope.editing) input.val(text);
        if (scope.render) text = scope.render(text);
        preview.html(text ? text : ' ');
            // Если нет текста, то у preview будет нулевой height
    } );

    scope.$on('$destroy', () => {
        input.off('keyup', handleKey);
        input.off('focusout', showPreview);
        preview.off('click', showInput);
    });

    // ...

    clone.insertAfter(element);


}


} () );
