( function () { 'use strict';


// Ожидает окончания загрузки всех данных, собирает их вместе и предоставляет 
// единым объектом для всех нуждающихся.


angular
    .module( 'counter.data.projectData', [
        'counter.data.userData',
        'counter.data.recordsData'
    ] )
    .service( 'projectData', Data );


function Data($q, userData, recordsData) {
    // data — комбинация загруженных другими модулями данных

    this.download = Function.once(
        () => $q.all({
                 user    : userData.download(),
                 records : recordsData.download()
              }).then( results => {
                  this.data = {
                      userName  : results.user.name,
                      order     : results.user.order,
                      propNames : results.user.propNames,
                      records   : results.records
                  };
                  return this.data;
              } )
    );

}


} () );
