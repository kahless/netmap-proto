var generators = require('yeoman-generator');


module.exports = generators.Base.extend({


    constructor: function () {
        generators.Base.apply(this, arguments);

        this.option('service', {
            alias: 's',
            type: Boolean,
            desc: 'Generate an angular service'
        });
        this.option('directive', {
            alias: 'd',
            type: Boolean,
            desc: 'Generate an angular directive'
        });
        this.option('controller', {
            alias: 'c',
            type: Boolean,
            desc: 'Generate an angular controller'
        });
        this.option('spec', {
            alias: 'S',
            type: Boolean,
            desc: 'Generate jasmine tests'
        });

        this.argument('filename', {
            type: String,
            required: true
        });
    },


    writing: function () {
        var types = ['service', 'directive', 'controller', 'spec'];
        var type = types.find( t => this.options[t] );
        this.copy(
            this.templatePath(type + '.js'),
            this.destinationPath(this.filename)
        );
    }


});

