( function () { 'use strict';


angular
    .module('layers.clusters.clusterModalController', [])
    .controller('ClusterModalController', ClusterModalController);


function ClusterModalController($uibModalInstance) {

    this.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    this.save = function () {
        var someval = true;
        $uibModalInstance.close(someval);
    };

}


} () );
