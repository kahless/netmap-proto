( function () { 'use strict';


angular
    .module('layers.clusters.clusterPopupController', [
        'layers.clusters.clusterModalController'
    ])
    .controller('ClusterPopupController', ClusterPopupController);


function ClusterPopupController($uibModal, src) {

    this.edit = function () {
        var modalInstance = $uibModal.open({
            templateUrl: `${src}/layers/clusters/cluster-modal.html`,
            controllerAs: 'modal',
            controller: 'ClusterModalController',
            animation: true
        });
        modalInstance.result.then( value => {
            // TODO
            this.value = value;
        } );
    };

}


} () );
