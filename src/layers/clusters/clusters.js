( function () { 'use strict';

// Инициализация кластеровна карте


angular
    .module('layers.clusters.clusters', [
        'config',
        'layers.clusters.clusterPopupController'
    ])
    .factory('ClusterLayers', clusterLayers);


function clusterLayers($rootScope, $templateRequest, $compile, src) {

    var markerIcon = L.icon({
        iconUrl: `${src}/layers/clusters/marker.png`,
        iconSize: [34, 34],
        iconAnchor: [17, 17],
        popupAnchor: [0, 0]
    });


    return function (clusters, clusterLinks, clusterLinksOpt) {
        var scope = $rootScope.$new();

        this.markers = clusters.map( cluster => {
            var marker = L.marker(cluster.coords, {
                icon: markerIcon,
                title: cluster.name
            });

            $templateRequest(`${src}/layers/clusters/cluster-popup.html`)
                .then( html => {
                    var linkFunction = $compile(jQuery(html));
                    var scope = $rootScope.$new();
                    scope.cluster = cluster;
                    var el = linkFunction(scope)[0];
                    marker.bindPopup(el);
                } );

            return marker
        } );

        this.links = clusterLinks.map( link => {
            var genLink = options => {
                return L.polyline(
                    [
                        clusters[link.tip1].coords,
                        clusters[link.tip2].coords
                    ],
                    Object.assign({weight: 3}, options)
                );
            };
            if (!link.double) {
                return genLink({ color: 'blue' });
            } else {
                return [
                    genLink({ color: 'red', offset: -2 }),
                    genLink({ color: 'green', offset: +3 })
                ];
            }
        } ).flatten();

        // Связи для опционального слоя
        this.linksOpt = clusterLinksOpt.map( link => {
            var genLink = options => {
                return L.polyline(
                    [
                        clusters[link.tip1].coords,
                        clusters[link.tip2].coords
                    ],
                    Object.assign({weight: 12}, options)
                );
            };
            return genLink({ color: 'orange' });
        } ).flatten();

    };


}


} () );
