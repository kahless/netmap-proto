( function () { 'use strict';


angular
    .module('layers.map', [
        'mapEvents',
        'data.model',
        'layers.clusters.clusters',
        'layers.controllers.controllers'
    ])
    .service('mapLayers', MapLayers);



function MapLayers(mapEvents, model, ClusterLayers, ControllerLayers) {


    var clusterDefaultZoom = 4;
    var controllerDefaultZoom = 12
    var clusterCenter = [58.010259, 56.234195]; // Начальное положение карты


    this.init = function (map) {

        map.setView(clusterCenter, clusterDefaultZoom);
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        // Кластеры
        var clusters = new ClusterLayers(
            model.clusters,
            model.clusterLinks,
            model.clusterLinksOpt
        );

        // Контроллеры
        var controllers = new ControllerLayers(
            model.clusters[0].controllers,
            model.clusters[0].linksOpt
        );

        var clustersLayer = L.layerGroup([
            ...clusters.markers,
            ...clusters.links
        ]);
        var clustersLayerOpt = L.layerGroup([
            ...clusters.linksOpt
        ]);
        var controllersLayer = L.layerGroup([
            ...controllers.markers,
            ...controllers.links
        ]);
        var controllersLayerOpt = L.layerGroup([
            ...controllers.linksOpt
        ]);
        clustersLayer.addTo(map);

        var layersControl = L.control.layers(null, {
            'Cluster optional layer': clustersLayerOpt
        });
        layersControl.addTo(map);

        map.on('zoomend', event => {
            var zoom = map.getZoom();
            if (zoom > 8) {
                //var x = map.hasLayer(clustersLayerOpt);
                layersControl.removeLayer(clustersLayerOpt);
                layersControl.addOverlay(
                    controllersLayerOpt,
                    'Controller optional layer'
                );
                map.removeLayer(clustersLayer);
                map.removeLayer(clustersLayerOpt);
                map.addLayer(controllersLayer);
            } else {
                layersControl.removeLayer(controllersLayerOpt);
                layersControl.addOverlay(
                    clustersLayerOpt,
                    'Cluster optional layer'
                );
                map.removeLayer(controllersLayer);
                map.removeLayer(controllersLayerOpt);
                map.addLayer(clustersLayer);
            }
        });

        mapEvents.setCenter.on( item => {
            map.setView(
                item.coords,
                item.name ? clusterDefaultZoom : controllerDefaultZoom
            );
        } );

    };


}


} () );
