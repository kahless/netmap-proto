( function () { 'use strict';

// Инициализация контроллеров на карте


angular
    .module('layers.controllers.controllers', [
        'config'
    ])
    .factory('ControllerLayers', controllerLayers);


function controllerLayers(src) {


    var markerIcon = L.icon({
        iconUrl: `${src}/layers/controllers/marker.png`,
        iconSize: [34, 34],
        iconAnchor: [17, 17],
        popupAnchor: [0, 0]
    });


    return function (controllers, controllerLinksOpt) {

        this.markers = controllers.map(
            ctl => L.marker(ctl.coords, {icon: markerIcon})
        );

        this.links = controllers.map(
            (ctl, i, ctls) => ctls.slice(i + 1).map(
                pair => L.polyline([ctl.coords, pair.coords])
            )
        ).flatten();

        this.linksOpt = controllerLinksOpt.map( link => {
            var genLink = options => {
                return L.polyline(
                    [
                        controllers[link.tip1].coords,
                        controllers[link.tip2].coords
                    ],
                    Object.assign({weight: 12}, options)
                );
            };
            return genLink({ color: 'orange' });
        } ).flatten();

    };


}


} () );
