( function () { 'use strict';


var mapEvents = new Observers(
    'setCenter' // Запрос центрироания карты на объекте
);


angular
    .module('mapEvents', [])
    .value('mapEvents', mapEvents);


} () );
