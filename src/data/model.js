( function () { 'use strict';


angular
    .module('data.model', [])
    .service('model', Model);


function Model() {

    this.clusters = [
        { coords: L.latLng(55.753742, 37.620983), name: 'Москва' },
        { coords: L.latLng(59.929678, 30.305844), name: 'Санкт-Петербург' },
        { coords: L.latLng(55.018181, 82.924005), name: 'Новосибирск' },
        { coords: L.latLng(56.838607, 60.605514), name: 'Екатеринбург' },
        { coords: L.latLng(56.326887, 44.005986), name: 'Нижний Новгород' }
    ];

    this.clusterLinks = [
        { tip1: 0, tip2: 1, double: true },
        { tip1: 0, tip2: 4 },
        { tip1: 4, tip2: 2, double: true },
        { tip1: 1, tip2: 4 },
        { tip1: 3, tip2: 2 }
    ];

    // Связи для опционального слоя
    this.clusterLinksOpt = [
        { tip1: 1, tip2: 4 },
        { tip1: 4, tip2: 2 }
    ];

    this.clusters[0].controllers = [
        { coords: L.latLng(55.675968, 37.743938), addr: '1.100.113.113' },
        { coords: L.latLng(55.717058, 37.752895), addr: '1.100.123.123' },
        { coords: L.latLng(55.666234, 37.492147), addr: '1.100.133.133' },
        { coords: L.latLng(55.808975, 37.537368), addr: '1.100.143.143' },
        { coords: L.latLng(55.787724, 37.703009), addr: '1.100.163.153' }
    ];

    // Связи для опционального слоя
    this.clusters[0].linksOpt = [
        { tip1: 3, tip2: 1 },
        { tip1: 1, tip2: 2 },
        { tip1: 2, tip2: 3 }
    ];

}


} () );
