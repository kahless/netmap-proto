( function () { 'use strict';


angular
    .module('map.mapDirective', [
        'layers.map'
    ])
    .directive('xxMap', xxMap);


function xxMap(mapLayers) {
    return {
        restrict: 'E',
        link(scope, element) {
            var map = L.map( element.get(0) );
            mapLayers.init(map);
        }
    };
}


} () );
