( function () { 'use strict';


angular
    .module('list.listController', [
        'mapEvents',
        'data.model'
    ])
    .controller('ListController', ListController);


function ListController(model, mapEvents) {

    this.items = model;

    this.setCenter = function (item) {
        mapEvents.setCenter.fire(item);
    };

}


} () );
