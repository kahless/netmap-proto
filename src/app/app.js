( function () { 'use strict';


angular
    .module('app.app', [
        'ui.bootstrap',
        'map.mapDirective',
        'list.listController',
        'layers.map'
    ]);


} () );
