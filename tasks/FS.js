'use strict';


module.exports = {

    base: '.',
    dist: 'dist',

    url: {
        st: '',     // URL for static dir
        src: '/src' // URL for src dir
    },

    gulpfiles: [
        'gulpfile.js',
        'gulp-tasks/**/*.js'
    ],

    app: {
        staticFiles: 'src/**/*.{png,jpeg,gif,svg,ico}',
        index: 'src/app/app.jade',
        jade: [
            'src/**/*.jade',
            '!src/app/app.jade'
        ],
        scss: 'src/**/*.scss',
        js: [
            'src/**/*.js',
            '!src/**/*.spec.js'
        ],
        ngConfig: 'config.js'
    },

    libs: {
        // Custom bootstrap build
        bootstrap: 'lib/bootstrap-custom/bootstrap.less',
        bootstrapMin: 'lib/bootstrap-custom/bootstrap.css'
    },

    packages: {
        npm: [
            'node_modules/babel-polyfill/dist/polyfill.js'
        ]
    }

};
