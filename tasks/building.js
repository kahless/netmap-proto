'use strict';


var gulp        = require('gulp');
var runSequence = require('run-sequence');
var del         = require('del');
var FS          = require('./FS.js');


gulp.task('clean', function () {
    del.sync([ FS.dist + '/*' ], { dot: true });
});


gulp.task('build', function () {
    runSequence(
        'clean',
        [
            'static',
            'index',
            'jade',
            'scss',
            'js',
            'ngConfig',
            'packages',
            'libs'
        ]
    );
});
