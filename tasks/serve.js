'use strict';


var runSequence = require('run-sequence');
var gulp        = require('gulp');
var watch       = require('gulp-watch');
var FS          = require('./FS.js');


gulp.task('serve', function () {
    runSequence('build', 'watch' );
});


gulp.task('watch', function () {
    watch( FS.app.index, function () { return gulp.start('index'); } );
    watch( FS.app.staticFiles, function () { return gulp.start('static'); } );
    watch( FS.app.jade, function () { return gulp.start('jade'); } );
    watch( FS.app.scss, function () { return gulp.start('scss', 'index'); } );
    watch( FS.app.js, function () { return gulp.start('js', 'index'); } );
    watch( 'bower.json', function () { return gulp.start('bower', 'index'); } );
});
