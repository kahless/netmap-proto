'use strict';


exports.errorHandler = function (error) {
    console.error('ERROR in plugin — ' + error.plugin);
    console.error(error.stack);
    console.error(error.message);
};
