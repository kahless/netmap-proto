'use strict';
// Extraction of useful filenames from bower_components


var path           = require('path');
var mainBowerFiles = require('main-bower-files');


module.exports = function (options) {

    // All the exported filenames from the bower_components
    if (!options) options = {};
    this.all = mainBowerFiles(
        Object.assign( { checkExistence: true }, options )
    );

    var base = path.join(__dirname + '/../../');
    this.all = this.all.map( function (f) { return f.replace(base, ''); } );
        // 'main-bower-files' always returns absolute paths

    this.getCSS = function () {
        return this.all.filter( function (f) {
            return /.css$/.test(f);
        } );
    };

    this.getJS = function () {
        return this.all.filter( function (f) {
            return /.js$/.test(f);
        } );
    };

    this.getStatic = function () {
        return this.all.filter( function (f) {
            return !/.(js|css)$/.test(f);
        } );
    };

};
