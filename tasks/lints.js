'use strict';


var gulp   = require('gulp');
var eslint = require('gulp-eslint');
var FS     = require('./FS.js');


var rules = {
    // {{{
    'semi': 2,
    'comma-dangle': 2,
    'no-cond-assign': 2,
    'no-dupe-args': 2,
    'no-dupe-keys': 2,
    'no-duplicate-case': 2,
    'no-empty-character-class': 2,
    'no-extra-semi': 2,
    'no-func-assign': 2,
    'no-invalid-regexp': 2,
    'use-isnan': 2,
    'valid-typeof': 2,
    'no-fallthrough': 2,
    'no-octal': 2,
    'no-delete-var': 2,
    'no-undef': 2,
    'no-mixed-spaces-and-tabs': 2,
    'no-irregular-whitespace': 0
    // }}}
};


var globals = {
    bowerComponents: {
        $          : false,
        jQuery     : false,
        angular    : false,
        cytoscape  : false
    },
    nodeLib: { },
    browserLib: { }
};


gulp.task('lint', ['lint-gulp', 'lint-app']);


gulp.task('lint-gulp', function () {
    return gulp.src(FS.gulpfiles)
        .pipe( eslint({
            rules: rules,
            envs: [ 'node', 'es6' ]
        }) )
        .pipe( eslint.format() );
});


gulp.task('lint-app', function () {
    return gulp.src(FS.app.js)
        .pipe( eslint({
            rules: rules,
            envs: [ 'browser', 'es6' ],
            globals: Object.assign( {},
                globals.bowerComponents,
                globals.browserLib
            )
        }) )
        .pipe( eslint.format() );
});
