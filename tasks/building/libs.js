'use strict';

// Building our libraries (from the 'libs' directory)
//
// This files are not packed with the other app files because app files may 
// depend on this libraries so they should be packed in a separate files that 
// could be required first.


var gulp = require('gulp');
var less = require('gulp-less');

var FS   = require('../FS.js');


gulp.task('libs', ['bootstrap']);


gulp.task('bootstrap', function () {
    return gulp.src(FS.libs.bootstrap, { base: '.' })
        .pipe( less({
            paths: ['bower_components/bootstrap/less']
        }) )
        .pipe( gulp.dest(FS.dist) );
});


