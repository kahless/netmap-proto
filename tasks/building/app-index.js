'use strict';
// Task for building index.html


var fs              = require('fs');
var streamToArray   = require('stream-to-array');

var gulp            = require('gulp');
var print           = require('gulp-print');
var plumber         = require('gulp-plumber');
var jade            = require('gulp-jade');

var errorHandler    = require('../lib/errorHandler.js');
var BowerComponents = require('../lib/bower-components.js');
var FS              = require('../FS.js');


// Obtains filenames by shell globs using gulp.src()
function filenames(globs) {
    return streamToArray(
        gulp.src( globs, { base: FS.base, read: false } )
    ).then( function (vinyls) {
        return vinyls.map( function (v) {
            return v.relative;
                // filename is stored in: relative, path, base
        } );
    } );
}


// Crops base directory from a filename
function cropBase(names) {
    var base = RegExp('^' + FS.base + '/');
    return names.map( function (n) {
        return n.replace(base, '');
    } );
}


gulp.task('index', function (done) {

    // Retrieving filenames that must be placed into the page
    var bowerComps = new BowerComponents();
    var gotLinks = Promise.all([
        filenames( FS.app.scss ),
        filenames( FS.app.js )
    ]).then( function (files) {
        var cssBower = cropBase( bowerComps.getCSS() );
        var scssApp  = files[0];
        var jsBower  = cropBase( bowerComps.getJS() );
        var jsApp    = files[1];
        scssApp = scssApp.map( function (f) {
            return f.replace(/\.scss/, '.css');
        } );
        return {
            css : cssBower.concat([FS.libs.bootstrapMin], scssApp),
            js  : jsBower.concat([FS.packages.npm, FS.app.ngConfig], jsApp)
        };
    });

    gotLinks.then( function (links) {
        gulp.src(FS.app.index, { base: FS.base })
            .pipe( plumber({ errorHandler: errorHandler }) )
            .pipe( jade({
                pretty: true,
                doctype: 'html', // Remove mirroring of 'translate' attribute
                locals: {
                    st    : FS.url.st,
                    src   : FS.url.src,
                    links : links
                }
            }) )
            .pipe( gulp.dest(FS.dist) );
        done();
    } );

});
