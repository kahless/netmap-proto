'use strict';

var path            = require('path');

var autoprefixer    = require('autoprefixer');
var postcssNested   = require('postcss-nested');
var postcssVars     = require('postcss-simple-vars');

var gulp            = require('gulp');
var print           = require('gulp-print');
var plumber         = require('gulp-plumber');
var babel           = require('gulp-babel');
var cache           = require('gulp-cached');
var sourcemaps      = require('gulp-sourcemaps');
var jade            = require('gulp-jade');
var postcss         = require('gulp-postcss');
var rename          = require('gulp-rename');
var ngConstant      = require('gulp-ng-constant');

var errorHandler    = require('../lib/errorHandler.js');
var FS              = require('../FS.js');

var sourcemapOpts = {
    debug: true,
    sourceRoot: '/__maps',
    sourceMappingURL: function (file) {
        return '/__maps/' + file.relative + '.map';
    }
};



gulp.task('jade', function () {
    return gulp.src(FS.app.jade, { base: FS.base })
        .pipe( plumber({ errorHandler: errorHandler }) )
        .pipe( cache('jade') )
        .pipe( jade({
            pretty: true,
            doctype: 'html', // Remove mirroring of 'translate' attribute
            locals: {
                st  : FS.url.st,
                src : FS.url.src
            }
        }) )
        .pipe( gulp.dest(FS.dist) );
});


gulp.task('js', function () {
    return gulp.src(FS.app.js, { base: FS.base })
        .pipe( plumber({ errorHandler: errorHandler }) )
        .pipe( cache('js') )
        .pipe( babel({
            plugins: [
                'transform-es2015-block-scoping', // let & const
                'transform-es2015-spread',
                'transform-es2015-arrow-functions',
                'transform-es2015-template-literals', // template strings
                'transform-es2015-shorthand-properties' // extended obj literals
            ]
        }) )
        .pipe( gulp.dest(FS.dist) );
});


gulp.task('scss', function () {
    return gulp.src(FS.app.scss, { base: FS.base })
        .pipe( plumber({ errorHandler: errorHandler }) )
        .pipe( cache('scss') )
        .pipe( sourcemaps.init() )
        .pipe( postcss([
            postcssNested,
            postcssVars({
                variables: {
                    //stHost: configs.dev.get('static.host')
                }
            }),
            autoprefixer({ browsers: ['last 2 versions'] })
        ]) )
        .pipe( rename( function (path) {
            // It is neccesary because postcss doesn't change file extension 
            // (scss->css) by its own but gulp-sourcemaps understands how to 
            // generate a map by extension.
            if ( !path.dirname.startsWith('__maps') ) {
                path.extname = '.css'; // *.scss -> *.css
            }
        }) )
        .pipe( sourcemaps.write('__maps', sourcemapOpts) )
        .pipe( gulp.dest(FS.dist) );
});


gulp.task('static', function () {
    return gulp.src(FS.app.staticFiles, { base: FS.base } )
        .pipe( cache('static') )
        .pipe( gulp.dest(FS.dist) );
});


gulp.task('ngConfig', function () {
    ngConstant({
        stream: true,
        name: 'config',
        constants: {
            st  : FS.url.st,
            src : FS.url.src
        }
    })
        .pipe( rename( function (file) {
            var parsed = path.parse(FS.app.ngConfig);
            file.basename = parsed.name;
        } ) )
        .pipe( gulp.dest(FS.dist) );
});
