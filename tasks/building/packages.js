'use strict';

// Building external packages from bower_components, node_modules and so on


var gulp            = require('gulp');
var cache           = require('gulp-cached');

var BowerComponents = require('../lib/bower-components.js');
var FS              = require('../FS.js');


gulp.task('packages', function () {
    var bowerComponents = new BowerComponents({});
    var src = bowerComponents.all.concat(FS.packages.npm);
    return gulp.src(src, { base: '.' })
        .pipe( cache('bower') )
        .pipe( gulp.dest(FS.dist) );
});
