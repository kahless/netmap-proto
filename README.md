## Прототип карты

Установка:

```
#!shell
npm install
npm run bower
```


Запуск:

```
#!shell
npm run server
npm start
```


Результат: [http:127.0.0.1:8000/src/app/app.html](http:127.0.0.1:8000/src/app/app.html)