'use strict';


var gulp = require('gulp');
var requireDir = require('require-dir');


require('babel-polyfill');
requireDir('./tasks', {recurse: true, duplicates: true});


gulp.task('default', ['serve']);
